public class HittingTheJackpot {
    public static void main(String[] args) {
        String[] elements = {"SS", "SS", "SS", "S5"};

        if (testJackpot(elements)) {
            System.out.println("Jackpot!");
        }
    }

    public static boolean testJackpot(String[] elements) {
        for (int i = 1; i < elements.length; i++) {
            if (!elements[i].equals(elements[i - 1])) {
                return false;
            }
        }
        return true;
    }
}
